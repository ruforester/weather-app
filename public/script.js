/// <reference path="../typings/main.d.ts" />

var geoData = {
    lat: 0,
    lon: 0,
    units: "metric",
    APPID: "54531dc4fcb71eee4f4beb11fb1212fb"
};

var tempF = '';
var tempC = '';
var C = 0;
var F = 0;


function setEventTempConverter() {


    tempF.on("click", function() {


        tempF.toggle();
        tempC.toggle();

    });
    tempC.on("click", function() {
        tempF.toggle();
        tempC.toggle();


    });
}




function getWeather() {

    $.getJSON("http://api.openweathermap.org/data/2.5/weather", geoData, function(data) {
        $("#weather").html(JSON.stringify(data));

        C = Math.floor(data['main']['temp']);
        // C = Math.floor((F - 32) * 5 / 9);
        F = Math.floor(C * 9 / 5 + 32);

        $("#city").html(data['name'].toUpperCase() + ', ' + data['sys']['country'].toUpperCase());
        tempF.html(F + '\xB0' + ' F');
        tempC.html(C + '\xB0' + ' C');
        setEventTempConverter();



        var iconCode = data["weather"][0]["icon"];
        var iconDayNight = iconCode.slice(2);

        $("#bg").css("background-image", "url('assets/images/" + (iconDayNight === 'd' ? iconCode.slice(0, 2) : 'n') + ".jpg')");
        $("#bg").fadeIn(800);
    });
} //end getWeather()



function getGeoIP() {

    $.ajax({
        url: "http://ip-api.com/json/",
        data: geoData,
        dataType: "json",
        success: function(jsonData) {
            geoData.lat = jsonData["lat"];
            geoData.lon = jsonData["lon"];
            getWeather();
        },
        error: function(err) {
            console.log(err.message);
        }
    });

} //end getGeoIP()



function geoSuccess(position) {
    geoData.lat = position.coords.latitude;
    geoData.lon = position.coords.longitude;
    $("#geo").html("latitude: " + geoData.lat + "<br> longitude: " + geoData.lon);
    getWeather();
};



//user denied geolocation in the browser
function geoError(err) {
    console.log(err.message);
    getGeoIP();

};


$(document).ready(function() {
    tempF = $("#tempF");
    tempC = $("#tempC");
    tempF.hide();

    navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
});